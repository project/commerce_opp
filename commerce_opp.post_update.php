<?php

/**
 * @file
 * Post update functions of commerce_opp module.
 */

/**
 * Update payments in authorization state.
 *
 * Populate opp_checkout_id field and adjust payment type, if necessary.
 */
function commerce_opp_post_update_1() {
  /** @var \Drupal\commerce_opp\OpenPaymentPlatformServiceInterface $opp_service */
  $opp_service = \Drupal::service('commerce_opp.opp_service');

  $opp_gateways = $opp_service->getOppGatewayIds(FALSE);
  if (empty($opp_gateways)) {
    return t('No Open Payment Platform gateways found.');
  }

  $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
  $query = $payment_storage->getQuery();
  $query->accessCheck(FALSE);
  $query->condition('payment_gateway', $opp_gateways, 'IN');
  $query->condition('state', 'authorization');
  $payments = $query->execute();
  if (empty($payments)) {
    return t('No Open Payment Platform payments in authorization state found.');
  }

  /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
  $payments = $payment_storage->loadMultiple($payments);
  foreach ($payments as $payment) {
    if ($payment->getType()->getPluginId() == 'payment_default') {
      $payment->type = 'opp';
      $payment->save();
      $payment = $payment_storage->load($payment->id());
    }
    $payment->set('opp_checkout_id', $payment->getRemoteId());
    $payment->save();
  }

  return t('Number of updated payments: @count', ['@count' => count($payments)]);
}

/**
 * Remove 'virtual_account_id' config setting from MBWAY gateway configurations.
 */
function commerce_opp_post_update_2() {
  $payment_gateway_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
  $query = $payment_gateway_storage->getQuery();
  $query->accessCheck(FALSE);
  $query->condition('plugin', 'opp_copyandpay_mbway');
  $ids = $query->execute();
  if (empty($ids)) {
    return t('No MBWAY configurations to process.');
  }
  foreach ($ids as $id) {
    \Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $id)
      ->clear('configuration.virtual_account_id')
      ->save();
  }

  return t('Number of updated configs: @count', ['@count' => count($ids)]);
}
