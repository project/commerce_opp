Commerce Open Payment Platform
===============
This module integrates [ACI PAY.ON Payments Gateway](https://docs.prtso.com)
(formerly known as "Open Payment Platform") with Drupal Commerce, integrating
the PAYFRAME (formerly known as COPYandPAY) payment widget in Drupal Commerce
checkout flow.

The ACI PAY.ON Payments Gateway is often used as white label solution and named
differently per country, eg. in Portugal it is offered by SIBS and called
"SIBS Payments", in Austria by Viveum and called "VIVEUM Meteorpay".

[Issue Tracker](https://www.drupal.org/project/issues/commerce_opp?version=8.x)

## Requirements

Commerce Open Payment Platform depends on Drupal Commerce of course, given a
strict dependency on commerce_payment sub module.

## Installation

It is recommended to use [Composer](https://getcomposer.org/) to get this module
with all dependencies:

```
composer require "drupal/commerce_opp"
```

See the [Drupal](https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies)
documentation for more details.

## Configuration

Create a new Open Payment Platform payment gateway:
  
Visit *Administration > Commerce > Configuration > Payment gateways > Add
payment gateway* and fill in the required fields. Use the API credentials
provided by your PAY.ON account. It is recommended to enter test credentials and
then override these with live credentials in settings.php. This way, live
credentials will not be stored in the DB. You also have to choose the brand(s)
(e.g. VISA) you want to support. 

There are 3 basic payment gateway types, you can choose from:

* Open Payment Platform PAYFRAME (bank transfer)
* Open Payment Platform PAYFRAME (credit cards)
* Open Payment Platform PAYFRAME (virtual accounts)

Additionally, some brands require special treatment, resulting in having
dedicated plugins for them. Currently these are:

* Open Payment Platform PAYFRAME: MBWAY (virtual account)
* Open Payment Platform PAYFRAME: SIBS MULTIBANCO (virtual account)
* Open Payment Platform PAYFRAME: SOFORTÜBERWEISUNG (bank transfer)

While you can choose multiple credit card brands for a single gateway instance,
you need to create a single instance for every bank or virtual account brand.
Why did we choose that approach? There are two main reasons for that:

1. Commerce 2.x currently does not provide additional customizing form options
   for payment gateways on the checkout page, meaning that you can't have one
   "credit card or bank transfer" entry, and then select the brand, before you
   proceed to the payment page.
2. On the payment page, PAYFRAME is only able to group credit cards together
   in one widgets. For any other brand that is defined on the payment page, a
   separate payment widget would be generated, which would cause a lot of
   confusion to your customers.

## Webhooks

### Introduction

Webhooks are HTTP callbacks that notify you of all events you subscribed for on
an entity. Events can be payments, state changes to payments or transactions
connected to a payment (e.g. a chargeback).

[API documentation](https://docs.oppwa.com/tutorials/webhooks)

### Configuration (payment provider side)

Steps on how to configure webhooks in the payment provider's backend can be
found [here](https://docs.oppwa.com/tutorials/webhooks/configuration).

Please note that in many cases, you won't have the necessary permissions enabled
to configure webhooks. If that's the case, get in touch with your payment
provider and you'll either get the necessary permissions, or you can ask them to
configure the webhooks for you.

Another interesting and important thing to know is, that you can define an
webhook globally (for all your configured brands), or for an specific brand
only - which will be mentioned in the recommendation section below.

This module defines a single endpoint, mapped to the path '/opp/webhooks'. So
the URL you have to enter is **https://YOURDOMAIN/opp/webhooks**.

**Notification types: please only check 'DB', 'CP' and 'RC' for type 'PAYMENT',
nothing else**, more info below ("Supported notification and payment types").

**Secret for encryption**: copy the proposed value or enter your own. If you
define multiple webhooks (eg one for each brand), please pay attention to use
the same secret for every webhook, as we only have a single endpoint on the
Drupal side, which needs to know which secret to use for decrypting the message.

**Wrapper for encrypted notification**: choose 'None', which is the default
value anyway.

**Emails**: it is recommended to enter at least one e-mail address to get
notifications, if webhook calls are failing.

#### Supported notification and payment types

Webhooks are available for PAYMENT, REGISTRATION and RISK notification types,
but not all of them are supported by this module. **Only PAYMENT type is
supported**. Webhooks of REGISTRATION or RISK type will trigger an exception,
resulting in a non-200 HTTP response code.

So please, only check 'DB', 'CP' and 'RC' PAYMENT types, nothing else.

### Configuration (in Drupal)

Go to the settings page at /admin/commerce/config/payment/opp and enter the
same value you have configured before as **Secret for encryption** in the
'Webhooks encryption secret' in Drupal and save the changes. That's the only
required setting on the Drupal side. There's however one related setting that
can be controlled per configured payment gateway plugin:

Each commerce_opp plugin has a **'Process pending authorizations on cron'
setting**, which is enabled by default. Without having a webhook defined, no
server-to-server notifications on a payment status will be sent, which means
that your Drupal site only gets notified on the customer's return on the
checkout complete page. If the customer however never reaches this page - which
can happen due to a lot of reasons, eg. browser closed before return, lost
internet connection, etc - the payment and so the order status would never get
updated. That's why this module actively queries the status of payments in
'authorization' state on cron run - for every configured commerce_opp plugin,
that has the 'Process pending authorizations on cron' setting enabled.

That means on the other hand, that you might want to disable this setting for
every brand, that you have configured an active webhook for. Disabling this
setting and using webhooks instead is a wise choice, if you are using brands
having a long authorization period (eg several days) like SIBS MULTIBANCO,
especially the higher your transaction/visitor count is.

### Recommendations (and limitations)

Currently, Drupal Commerce has a problem with concurrent calls of visiting the
checkout complete page and a server-to-server notification taking place at the
same time. Under certain circumstances, it may happen that an order gets double
placed: the order itself won't get duplicated, but it will result in a missing
order number (order number generation will be called twice), e-mail receipt
will be sent twice, and often stale order data is saved (eg having a missing
paid amount). Some payment gateways seem to not have this problem at all (it
always depends on the timing, when the notifications are sent), while others
having this problem frequently. Modules reported to have this problem too
include commerce_mollie and commerce_sofortbanking for example.

Several issue reports covering this topic exist. We are aware of at least these:
* https://www.drupal.org/project/commerce/issues/2656818
* https://www.drupal.org/project/commerce/issues/3043180
* https://www.drupal.org/project/commerce/issues/3085805

However, we have found a workaround to circumvent that problem, which is shipped
with 8.x-1.0-rc14. We are using the Drupal locking layer to prevent parallel
payment status modification, hence no order modifications. We are releasing the
lock very late - on terminating the request - in order to guarantuee that the
order state update can't collide later within the same request (when order is
updated outside of our code base).

## Known brand usages

The payment gateway supports a huge number of brands, including several local
ones. Although most of them should work out of the box with the generic plugins,
some of them need a special treatment and thus a dedicated plugin, eg. the 
above mentioned SIBS Multibanco. Here's a list of brands, which have confirmed
actual usages.

* eps-Überweisung
* Mastercard
* MB WAY
* PayPal
* SIBS MULTIBANCO
* SOFORT Überweisung
* VISA

Please feel free to contribute and report usages of other brands.

## Credits
Commerce Open Payment Platform module was originally developed and is currently
maintained by [Mag. Andreas Mayr](https://www.drupal.org/u/agoradesign).

All initial development was sponsored by
[agoraDesign KG](https://www.agoradesign.at).
