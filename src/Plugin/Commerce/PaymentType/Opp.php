<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the default payment type for OPP payments.
 *
 * @CommercePaymentType(
 *   id = "opp",
 *   label = "Open Payment Platform",
 * )
 */
class Opp extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['opp_checkout_id'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Checkout ID'))
      ->setDescription($this->t('The Open Payment Platform checkout ID.'))
      ->setRequired(FALSE);

    return $fields;
  }

}
