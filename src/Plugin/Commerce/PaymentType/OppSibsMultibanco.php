<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentType;

use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment type for SIBS_MULTIBANCO payments.
 *
 * @CommercePaymentType(
 *   id = "opp_sibs_multibanco",
 *   label = "SIBS MULTIBANCO",
 * )
 */
class OppSibsMultibanco extends Opp {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['pmt_ref'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Payment reference'))
      ->setDescription($this->t('The payment reference the customer needs to complete payment.'))
      ->setRequired(FALSE);

    return $fields;
  }

}
