<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the Open Payment Platform payment gateway.
 */
interface CopyAndPayInterface extends OffsitePaymentGatewayInterface, SupportsRefundsInterface {

  /**
   * The default live environment host.
   *
   * @var string
   */
  const DEFAULT_HOST_LIVE = 'https://eu-prod.oppwa.com';

  /**
   * The default test environment host.
   *
   * @var string
   */
  const DEFAULT_HOST_TEST = 'https://eu-test.oppwa.com';

  /**
   * Prepares the checkout by fetching a checkout ID from the payment gateway.
   *
   * @param string[] $params
   *   The parameters for initializing a payment. Should at least contain
   *   'paymentType', 'amount', 'currency' and 'descriptor'.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface|null $payment
   *   The payment entity. Can be used as a source of extra information for
   *   very special brand implementations (such as SIBS MULTIBANCO). The default
   *   ones don't really need this information, the $params should already
   *   include all payment related information.
   *
   * @return string
   *   A valid checkout ID as provided by Open Payment Platform gateway.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   If there's no checkout ID returned, or a request error occurred.
   */
  public function prepareCheckout(array $params = [], PaymentInterface $payment = NULL);

  /**
   * Checks the transaction status and updates payment, if necessary.
   *
   * The default implementation will use ::getTransactionReport() for querying
   * the transaction status, as this is more bulletproof than using
   * ::getCheckoutStatus(), which should only be used on the thank you page.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return bool
   *   TRUE, if the transaction status could have been fetched and updated to
   *   its final state, FALSE otherwise. This means, FALSE is returned, if the
   *   transaction status could not be fetched at all, as well as if the payment
   *   is still in pending or an unknown state. TRUE will only be returned, if
   *   the payment has either been completed or voided.
   */
  public function checkAndUpdateTransactionStatus(PaymentInterface $payment);

  /**
   * Fetches the transaction status for the given payment entity by checkout ID.
   *
   * The function is using the following endpoint:
   *   resourcePath=/v1/checkouts/{checkoutId}/payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity (having the checkout ID set as remote ID).
   *
   * @return \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface
   *   The transaction status.
   */
  public function getCheckoutStatus(PaymentInterface $payment);

  /**
   * Processes the transaction status for the given payment entity.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity (having the checkout ID set as remote ID).
   * @param array $transaction_status
   *   The transaction status that is given as message payload in webhook calls,
   *   as well as transaction queries - it is never meant to be called with
   *   manually created data.
   *
   * @return \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface
   *   The transaction status.
   */
  public function processTransactionStatus(PaymentInterface $payment, array $transaction_status);

  /**
   * Looks up into the session for stored transaction status for the payment.
   *
   * For specific brands (MB WAY at the moment only), we need to redirect the
   * user to a custom route, checking the payment status regularly, until we
   * can either place or abort the order. OPP throttles querying the same
   * payment ID to twice a minute. Therefore we'll want to store successful
   * transaction information in the session, so that we do not need to send
   * another query, if the page gets refreshed.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity (having the checkout ID set as remote ID).
   *
   * @return \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface|null
   *   The transaction status, if exists already in the session. NULL otherwise.
   */
  public function getPaymentStatusFromSession(PaymentInterface $payment);

  /**
   * Saves the transaction status in the session for the given payment.
   *
   * For specific brands (MB WAY at the moment only), we need to redirect the
   * user to a custom route, checking the payment status regularly, until we
   * can either place or abort the order. OPP throttles querying the same
   * payment ID to twice a minute. Therefore we'll want to store successful
   * transaction information in the session, so that we do not need to send
   * another query, if the page gets refreshed.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface $payment_status
   *   The OPP transaction/payment status.
   */
  public function savePaymentStatusInSession(PaymentInterface $payment, TransactionStatusInterface $payment_status);

  /**
   * Gets the transaction report for the given payment.
   *
   * This is done by querying the /v1/query endpoint.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return array
   *   The JSON response of the transaction query.
   */
  public function getTransactionReport(PaymentInterface $payment);

  /**
   * Returns the active gateway url, depending on the active mode (test, live).
   *
   * @return string
   *   The active gateway url. The active mode (test, live) is considered.
   */
  public function getActiveHostUrl();

  /**
   * Returns the configured gateway url for live mode.
   *
   * @return string
   *   The configured gateway url for live mode.
   */
  public function getLiveHostUrl();

  /**
   * Returns the configured gateway url for test mode.
   *
   * @return string
   *   The configured gateway url for test mode.
   */
  public function getTestHostUrl();

  /**
   * Returns a list of all currently configured brand IDs.
   *
   * @return string[]
   *   A list of all currently configured brand IDs.
   */
  public function getBrandIds();

  /**
   * Returns a list of all currently configured brands.
   *
   * @return \Drupal\commerce_opp\Brand[]
   *   A list of all currently configured brands.
   */
  public function getBrands();

  /**
   * Gets the paymentType parameter to use.
   *
   * In most cases, 'DB' for debit will be returned. Some brands will need 'PA'
   * instead.
   *
   * @return string
   *   The paymentType parameter to use.
   */
  public function getPaymentTypeParameter();

  /**
   * Returns whether to show the amount to be paid on the payment page.
   *
   * @return bool
   *   TRUE, if the gateway is configured to show the payable amount on the
   *   payment page, FALSE otherwise.
   */
  public function isAmountVisible();

  /**
   * Returns the order property used as merchant invoice ID fallback.
   *
   * At times of preparing the checkout, we usually do not have already an
   * order number generated (unless you set the order number before placement in
   * a custom module). Therefore, we have to define a fallback value for the
   * merchantInvoiceId. This function returns, whether we should use the uuid or
   * the order ID for that.
   *
   * @return string
   *   The order property used as merchant invoice ID fallback.
   */
  public function getMerchantInvoiceIdFallback(): string;

  /**
   * Returns the order property used as merchant transaction ID.
   *
   * We support the id as well as the uuid of the payment entity. uuid is the
   * new default as of rc21, but we need to support the old id based approach
   * as well. Therefor we need the configuration option. On 99% of the sites,
   * there won't be a functional difference. If you re-use the same payment
   * gateway on multiple sites, using the payment ID as identifier will lead to
   * problems naturally.
   *
   * @return string
   *   The order property used as merchant invoice ID fallback.
   */
  public function getMerchantTransactionIdField(): string;

  /**
   * Calculates the expiration time of a checkout ID based on a given timestamp.
   *
   * @param int|null $request_time
   *   A timestamp, or NULL. If NULL, the current request timestamp will be
   *   used.
   *
   * @return int
   *   The expiration timestamp.
   */
  public function calculateCheckoutIdExpireTime($request_time = NULL);

  /**
   * Returns the payable amount for the given order.
   *
   * By default, this is exactly the order total, ensuring a decimal precision
   * of exact two digits, as this is required by Open Payment Platform.
   *
   * Further, it gives other modules the possibility to alter this price, e.g.
   * it allows custom modules to provide early payment discounts, that do not
   * change the original order total value.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_price\Price
   *   The calculated payment amount to charge.
   */
  public function getPayableAmount(OrderInterface $order);

  /**
   * Loads a payment by its OPP checkout ID.
   *
   * @param string $opp_checkout_id
   *   The OPP checkout ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity, if found.
   */
  public function loadPaymentByCheckoutId($opp_checkout_id);

}
