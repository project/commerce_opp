<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

/**
 * Provides the Open Payment Platform PAYFRAME gateway for virtual accounts.
 *
 * @CommercePaymentGateway(
 *   id = "opp_copyandpay_virtual",
 *   label = "Open Payment Platform PAYFRAME (virtual accounts)",
 *   display_label = "Virtual account",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_opp\PluginForm\CopyAndPayForm",
 *   },
 *   payment_type = "opp",
 *   payment_method_types = {"paypal"},
 *   requires_billing_information = FALSE,
 * )
 */
class CopyAndPayVirtualAccount extends CopyAndPayBase {

  /**
   * {@inheritdoc}
   */
  protected function getBrandOptions() {
    return $this->brandRepository->getVirtualAccountBrandLabels();
  }

}
