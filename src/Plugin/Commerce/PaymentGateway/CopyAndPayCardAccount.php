<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

/**
 * Provides the Open Payment Platform PAYFRAME gateway for credit cards.
 *
 * @CommercePaymentGateway(
 *   id = "opp_copyandpay_card",
 *   label = "Open Payment Platform PAYFRAME (credit cards)",
 *   display_label = "Credit card",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_opp\PluginForm\CopyAndPayForm",
 *   },
 *   payment_type = "opp",
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex",
 *     "dinersclub",
 *     "discover",
 *     "jcb",
 *     "maestro",
 *     "mastercard",
 *     "unionpay",
 *     "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class CopyAndPayCardAccount extends CopyAndPayBase {

  /**
   * {@inheritdoc}
   */
  protected function allowMultipleBrands() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBrandOptions() {
    return $this->brandRepository->getCardAccountBrandLabels();
  }

}
