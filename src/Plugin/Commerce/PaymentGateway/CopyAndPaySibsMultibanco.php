<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_opp\Transaction\Status\Rejected;
use Drupal\commerce_opp\Transaction\Status\SuccessOrPending;
use Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a dedicated PAYFRAME gateway for SIBS MULTIBANCO virtual account.
 *
 * @CommercePaymentGateway(
 *   id = "opp_copyandpay_sibs_multibanco",
 *   label = "Open Payment Platform PAYFRAME: SIBS MULTIBANCO",
 *   display_label = "SIBS MULTIBANCO",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_opp\PluginForm\CopyAndPayForm",
 *   },
 *   payment_type = "opp_sibs_multibanco",
 *   requires_billing_information = FALSE,
 * )
 */
class CopyAndPaySibsMultibanco extends CopyAndPayBase implements HasPaymentInstructionsInterface {

  /**
   * The date format used for sending date values to SIBS.
   *
   * This is currently used for these tow custom parameters:
   *   - SIBSMULTIBANCO_RefIntlDtTm
   *   - SIBSMULTIBANCO_RefLmtDtTm.
   *
   * @var string
   */
  const DATE_FORMAT = 'Y-m-d\TH:i:s.vP';

  /**
   * The date format used for parsing incoming date values from SIBS.
   *
   * The only reason, why this is different to the DATE_FORMAT constant defined
   * above, is the PHP restriction, that 'v' is an accepted format for
   * formatting date values as milliseconds, but unfortunately is not
   * recognized by \DateTime::createFromFormat(), which is very inconvenient and
   * will be hopefully fixed in future PHP releases.
   *
   * @var string
   */
  const DATE_FORMAT_PARSING = 'Y-m-d\TH:i:s.uP';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pa_expiration_period' => 3600,
      'sibs_payment_entity' => '',
      'test_mode' => 'EXTERNAL',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['brands']['#access'] = FALSE;

    $form['pa_expiration_period'] = [
      '#type' => 'number',
      '#title' => $this->t('PA expiration time'),
      '#description' => $this->t('Expiration time of PA on SIBS side (in seconds).'),
      '#default_value' => $this->configuration['pa_expiration_period'],
    ];

    $form['sibs_payment_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SIBS Payment entity'),
      '#description' => $this->t('Payment entity to be provided by SIBS.'),
      '#default_value' => $this->configuration['sibs_payment_entity'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['pa_expiration_period'] = $values['pa_expiration_period'];
      $this->configuration['sibs_payment_entity'] = $values['sibs_payment_entity'];
      $this->configuration['brands'] = ['SIBS_MULTIBANCO'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCheckout(array $params = [], PaymentInterface $payment = NULL) {
    if (!empty($this->configuration['sibs_payment_entity'])) {
      $params['customParameters[SIBSMULTIBANCO_PtmntEntty]'] = $this->configuration['sibs_payment_entity'];
    }

    if (!empty($this->configuration['pa_expiration_period'])) {
      $now = $this->time->getRequestTime();
      $expires = $now + $this->configuration['pa_expiration_period'];
      $params['customParameters[SIBSMULTIBANCO_RefIntlDtTm]'] = $this->dateFormatter->format($now, 'custom', self::DATE_FORMAT);
      $params['customParameters[SIBSMULTIBANCO_RefLmtDtTm]'] = $this->dateFormatter->format($expires, 'custom', self::DATE_FORMAT);
    }

    if ($this->getMode() == 'test') {
      $params['customParameters[SIBS_ENV]'] = 'QLY';
    }

    return parent::prepareCheckout($params, $payment);
  }

  /**
   * {@inheritdoc}
   */
  protected function alterTransactionStatus(TransactionStatusInterface $transaction_status, PaymentInterface $payment, array $json_response) {
    if ($transaction_status instanceof SuccessOrPending) {
      // As this function is called both on returning to thank you page, as well
      // as on cron when processing payments in authorization state, it's very
      // important that we check the paymentType here. On placing the order, the
      // payment is in pre-authorization state, and we need to save the payment
      // reference (and show it to the customer later) and update the expires
      // time. When processing payments on cron on the other side, we will only
      // process receipts here. In that case however, we don't need to alter
      // transaction status here, CopyAndPayBase::processTransactionStatus()
      // already processes everything we need.
      switch ($json_response['paymentType']) {
        case 'PA':
          // The payment is in pre-authorization state. We need to get the
          // payment reference and save it to the payment entity, so that we can
          // show it to the customer on the order confirmation screen.
          if (!empty($json_response['resultDetails']['pmtRef']) &&
            $json_response['resultDetails']['AcquirerResponse'] == 'APPR') {

            $payment_ref_id = $json_response['resultDetails']['pmtRef'];
            $payment->set('pmt_ref', $payment_ref_id);

            $request_time = $this->time->getRequestTime();
            $payment->setExpiresTime($request_time + $this->configuration['pa_expiration_period']);
            if (!empty($json_response['resultDetails']['refLmtDtTm'])) {
              $expireDateTime = \DateTime::createFromFormat(self::DATE_FORMAT_PARSING, $json_response['resultDetails']['refLmtDtTm']);
              if ($expireDateTime) {
                $payment->setExpiresTime($expireDateTime->getTimestamp());
              }
              else {
                $this->logger->warning('refLmtDtTm parameter of SIBS_MULTIBANCO transaction status seems not to be in @format date format: @refLmtDtTm (remote payment ID: @remote_id)', [
                  '@format' => self::DATE_FORMAT_PARSING,
                  '@refLmtDtTm' => $json_response['resultDetails']['refLmtDtTm'],
                  '@remote_id' => $json_response['id'],
                ]);
              }
            }
            else {
              $this->logger->warning('SIBS_MULTIBANCO transaction status is missing the refLmtDtTm parameter (remote payment ID: @remote_id)', [
                '@remote_id' => $json_response['id'],
              ]);
            }
          }
          else {
            throw new InvalidResponseException($this->t('No payment reference ID set for @checkout_id or acquirer response is not APPR.', [
              '@checkout_id' => $payment->opp_checkout_id->value,
            ]));
          }
          break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function onReturnAction(PaymentInterface $payment, TransactionStatusInterface $payment_status) {
    $payment->setRemoteId($payment_status->getId());
    $payment->setRemoteState($payment_status->getCode());
    if ($payment_status instanceof SuccessOrPending) {
      $this->cleanupUnusedAuthorizations($payment->getOrder(), $payment->id());
      $this->checkAndUpdateTransactionStatus($payment);
      $payment->save();
    }
    elseif ($payment_status instanceof Rejected) {
      $payment->getState()->applyTransitionById('void');
      $payment->save();
      throw new PaymentGatewayException($this->t('We could not complete your payment. Please try again or contact us if the problem persists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];

    if (!$payment->get('pmt_ref')->isEmpty()) {
      $instructions = [
        '#theme' => 'sibs_multibanco_instructions',
        '#pmt_ref' => $payment->pmt_ref->value,
        '#amount' => $payment->getAmount(),
        '#validity_timestamp' => $payment->getExpiresTime(),
        '#sibs_payment_entity' => $this->configuration['sibs_payment_entity'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBrandOptions() {
    $brand = $this->brandRepository->getBrand('SIBS_MULTIBANCO');
    return [$brand->getId() => $brand->getLabel()];
  }

}
