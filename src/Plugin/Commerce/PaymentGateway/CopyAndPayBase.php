<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_opp\Brand;
use Drupal\commerce_opp\BrandRepositoryInterface;
use Drupal\commerce_opp\Event\AlterPaymentAmountEvent;
use Drupal\commerce_opp\Event\OpenPaymentPlatformPaymentEvents;
use Drupal\commerce_opp\OrderLockInterface;
use Drupal\commerce_opp\Transaction\Status\Factory;
use Drupal\commerce_opp\Transaction\Status\Pending;
use Drupal\commerce_opp\Transaction\Status\Rejected;
use Drupal\commerce_opp\Transaction\Status\Success;
use Drupal\commerce_opp\Transaction\Status\SuccessNeedingReview;
use Drupal\commerce_opp\Transaction\Status\SuccessOrPending;
use Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a base class for PAYFRAME payment gateways.
 *
 * For the sake of stricter configuration, we define different payment gateway
 * plugins for each of card, bank and virtual account brands. The differences
 * however only affect configuration options and class annotations.
 */
abstract class CopyAndPayBase extends OffsitePaymentGatewayBase implements CopyAndPayInterface {

  /**
   * The brand repository.
   *
   * @var \Drupal\commerce_opp\BrandRepositoryInterface
   */
  protected $brandRepository;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The order lock service.
   *
   * @var \Drupal\commerce_opp\OrderLockInterface
   */
  protected $orderLock;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new CopyAndPayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param \Drupal\commerce_opp\BrandRepositoryInterface $brand_repository
   *   The brand repository.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\commerce_opp\OrderLockInterface $order_lock
   *   The order lock service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter, EventDispatcherInterface $event_dispatcher, Client $http_client, BrandRepositoryInterface $brand_repository, RounderInterface $rounder, PrivateTempStoreFactory $private_temp_store_factory, DateFormatterInterface $date_formatter, LoggerChannelFactoryInterface $logger_channel_factory, OrderLockInterface $order_lock, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->brandRepository = $brand_repository;
    $this->eventDispatcher = $event_dispatcher;
    $this->httpClient = $http_client;
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->privateTempStore = $private_temp_store_factory->get('commerce_opp');
    $this->rounder = $rounder;
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger_channel_factory->get('commerce_opp');
    $this->orderLock = $order_lock;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('event_dispatcher'),
      $container->get('http_client'),
      $container->get('commerce_opp.brand_repository'),
      $container->get('commerce_price.rounder'),
      $container->get('tempstore.private'),
      $container->get('date.formatter'),
      $container->get('logger.factory'),
      $container->get('commerce_opp.order_lock'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_id' => '',
      'access_token' => '',
      'show_amount' => TRUE,
      'merchant_invoice_id_fallback' => 'id',
      'merchant_transaction_id_field' => 'uuid',
      'host_live' => CopyAndPayInterface::DEFAULT_HOST_LIVE,
      'host_test' => CopyAndPayInterface::DEFAULT_HOST_TEST,
      'test_mode' => 'INTERNAL',
      'brands' => [],
      'cron_delete_expired_authorizations' => TRUE,
      'cron_process_pending_authorizations' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The entity for the request'),
      '#description' => $this->t("By default this is the channel's ID. It can be the division, merchant or channel identifier. Division is for requesting registrations only, merchant only in combination with channel dispatching, i.e. channel is the default for sending payment transactions."),
      '#default_value' => $this->configuration['entity_id'],
      '#required' => TRUE,
    ];

    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#default_value' => $this->configuration['access_token'],
      '#required' => TRUE,
    ];

    $form['show_amount'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show amount to be paid on payment page'),
      '#default_value' => $this->configuration['show_amount'],
    ];
    
    $form['merchant_invoice_id_fallback'] = [
      '#type' => 'select',
      '#title' => $this->t('Merchant invoice ID fallback'),
      '#description' => $this->t('Choose which order property should be used as merchantInvoiceId, if the order number is not generated at that point (which usually is not, unless you set the order number already before placement.).'),
      '#options' => [
        'id' => $this->t('ID'),
        'uuid' => $this->t('UUID'),
      ],
      '#default_value' => $this->configuration['merchant_invoice_id_fallback'] ?? 'id',
      '#required' => TRUE,
    ];

    $form['merchant_transaction_id_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Merchant transaction ID field'),
      '#description' => $this->t('Choose which order property should be used as merchantTransactionId.'),
      '#options' => [
        'uuid' => $this->t('UUID'),
        'id' => $this->t('ID'),
      ],
      '#default_value' => $this->configuration['merchant_transaction_id_field'] ?? 'uuid',
      '#required' => TRUE,
    ];

    $brand_description = !$this->allowMultipleBrands() ? $this->t('If you want to support multiple brands, you need to configure separate gateways for each brand, as otherwise multiple PAYFRAME widgets would be rendered one after another on the payment page, which is very confusing. It is better to select the payment  method first in the checkout step and have a single widget rendered on the payment page then.') : '';
    $form['brands'] = [
      '#type' => 'details',
      '#title' => $this->t('Supported brands'),
      '#description' => $brand_description ? '<p>' . $brand_description . '</p>' : '',
      '#open' => TRUE,
    ];

    $form['brands']['brands'] = [
      '#type' => 'select',
      '#title' => $this->t('Supported brands'),
      '#required' => TRUE,
      '#multiple' => $this->allowMultipleBrands(),
      '#options' => $this->getBrandOptions(),
      '#default_value' => $this->configuration['brands'] ?? '',
      '#empty_value' => '',
      '#attributes' => ['size' => 10],
    ];

    $form['host'] = [
      '#type' => 'details',
      '#title' => $this->t("Gateway base URLs"),
      '#open' => FALSE,
    ];

    $form['host']['host_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL (live environment)'),
      '#description' => $this->t('The host URL for the live environment (defaults to %host_url)', ['%host_url' => CopyAndPayInterface::DEFAULT_HOST_LIVE]),
      '#default_value' => $this->getLiveHostUrl(),
      '#required' => TRUE,
    ];

    $form['host']['host_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host URL (test environment)'),
      '#description' => $this->t('The host URL for the test environment (defaults to %host_url)', ['%host_url' => CopyAndPayInterface::DEFAULT_HOST_TEST]),
      '#default_value' => $this->getTestHostUrl(),
      '#required' => TRUE,
    ];

    $form['host']['test_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Test mode'),
      '#description' => $this->t('"Internal" causes transactions to be sent to our simulators, which is useful when switching to the live endpoint for connectivity testing. "External" causes test transactions to be forwarded to the processor\'s test system for \'end-to-end\' testing'),
      '#options' => [
        'INTERNAL' => $this->t('Internal'),
        'EXTERNAL' => $this->t('External'),
      ],
      '#default_value' => $this->configuration['test_mode'] ?? 'INTERNAL',
      '#required' => TRUE,
    ];

    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t("Cron settings"),
      '#open' => FALSE,
    ];

    $form['cron']['cron_delete_expired_authorizations'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['cron_delete_expired_authorizations'],
      '#title' => $this->t('Delete expired authorizations on cron'),
      '#description' => $this->t('Every time, an Open Payment Platform plugin is selected on checkout and the payment page is visited then, a new payment entity with state "authorization" is created and saved. If the customer does not complete the payment, or is uncertain and jumps back and forth in the checkout process, a lot of unneeded data will be generated and stored then. If checked, expired Open Payment Platform authorizations will be deleted on cron (recommended).'),
    ];

    $form['cron']['cron_process_pending_authorizations'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['cron_process_pending_authorizations'],
      '#title' => $this->t('Process pending authorizations on cron'),
      '#description' => $this->t("Unless webhooks are configured, Open Payment Platform does not actively send notification callbacks. Instead they only send the checkout ID alongside calling the return URL. This means, if for whatever reason the customer doesn't get redirected back to the payment return page, we'll never get notified about the transaction status, leading to unplaced and locked orders, although the payment was most likely successful. If you do not use webhooks, turning this setting on is heavily recommended!"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $brands = $values['brands']['brands'];
      if (!empty($brands) && !is_array($brands)) {
        $brands = [$brands];
      }
      $this->configuration['entity_id'] = $values['entity_id'];
      $this->configuration['access_token'] = $values['access_token'];
      $this->configuration['show_amount'] = $values['show_amount'];
      $this->configuration['merchant_invoice_id_fallback'] = $values['merchant_invoice_id_fallback'];
      $this->configuration['merchant_transaction_id_field'] = $values['merchant_transaction_id_field'];
      $this->configuration['brands'] = $brands;
      $this->configuration['host_live'] = $values['host']['host_live'];
      $this->configuration['host_test'] = $values['host']['host_test'];
      $this->configuration['test_mode'] = $values['host']['test_mode'];
      $this->configuration['cron_delete_expired_authorizations'] = (bool) $values['cron']['cron_delete_expired_authorizations'];
      $this->configuration['cron_process_pending_authorizations'] = (bool) $values['cron']['cron_process_pending_authorizations'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCheckout(array $params = [], PaymentInterface $payment = NULL) {
    $checkout_id = NULL;
    $base_url = $this->getActiveHostUrl();
    $url = $base_url . '/v1/checkouts';
    if ($this->getMode() == 'test') {
      $params['testMode'] = $this->configuration['test_mode'];
    }
    $request_options = $this->getHttpClientRequestOptions(TRUE, $params);
    try {
      $response = $this->httpClient->post($url, $request_options);
      $json_response = json_decode($response->getBody(), TRUE);
      if (!empty($json_response['id'])) {
        $checkout_id = $json_response['id'];
        if ($payment) {
          $payment->set('opp_checkout_id', $checkout_id);
        }
      }
      else {
        throw new InvalidRequestException($this->t('Cannot prepare OPP checkout: could not retrieve checkout ID.'));
      }
    }
    catch (RequestException $request_exception) {
      throw new InvalidResponseException($this->t('Cannot prepare OPP checkout due to exception: @error', ['@error' => $request_exception->getMessage()]));
    }
    catch (\Exception $ex) {
      throw new InvalidResponseException($this->t('Cannot prepare OPP checkout due to exception: @error', ['@error' => $ex->getMessage()]));
    }
    return $checkout_id;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAndUpdateTransactionStatus(PaymentInterface $payment) {
    try {
      $transaction_report = $this->getTransactionReport($payment);
      // Restrict the payment types to debit, capture and receipt of payments.
      $allowed_payment_types = ['DB', 'CP', 'RC'];
      foreach ($transaction_report['payments'] as $payment_info) {
        if (!in_array($payment_info['paymentType'], $allowed_payment_types)) {
          continue;
        }
        $payment_status = $this->processTransactionStatus($payment, $payment_info);
        break;
      }
    }
    catch (\Exception $ex) {
      $this->logger->error($ex->getMessage());
      // Early exit on exception.
      return FALSE;
    }

    if (empty($payment_status)) {
      // Early exit on not having a final transaction status.
      return FALSE;
    }

    if ($payment_status instanceof Pending) {
      $this->logger->info('Skip processing of payment ID @payment_id, as it is in pending state (status code @status_code).', [
        '@payment_id' => $payment->id(),
        '@status_code' => $payment_status->getCode(),
      ]);
      return FALSE;
    }

    $successful_status_update = FALSE;

    // The ID we receive in this response is different to the checkout ID.
    // The checkout ID was only a temporary remote value, in order to be
    // able to fetch the payment in this callback. Now, we have received the
    // real remote ID and will use it.
    $payment->setRemoteId($payment_status->getId());
    $payment->setRemoteState($payment_status->getCode());
    if ($payment_status instanceof Success || $payment_status instanceof SuccessNeedingReview) {
      $payment->getState()->applyTransitionById('capture');
      $payment->save();
      $successful_status_update = TRUE;
    }
    elseif ($payment_status instanceof Rejected) {
      $payment->getState()->applyTransitionById('void');
      $payment->save();
      $successful_status_update = TRUE;
    }
    return $successful_status_update;
  }

  /**
   * {@inheritdoc}
   */
  public function getCheckoutStatus(PaymentInterface $payment) {
    if (!$payment->hasField('opp_checkout_id') || $payment->get('opp_checkout_id')->isEmpty()) {
      $this->orderLock->release($payment->getOrder());
      throw new \InvalidArgumentException('The given payment entity has no remote ID set - cannot check OPP transaction status therefore.');
    }

    $checkout_id = $payment->opp_checkout_id->value;
    $base_url = $payment->getPaymentGatewayMode() == 'live' ? $this->getLiveHostUrl() : $this->getTestHostUrl();
    $url = $base_url . '/v1/checkouts/' . $checkout_id . '/payment';
    $request_options = $this->getHttpClientRequestOptions(FALSE);
    try {
      $response = $this->httpClient->get($url, $request_options);
      $json_response = json_decode($response->getBody(), TRUE);
      if (empty($json_response['id'])) {
        $error_description = !empty($json_response['result']['description']) ? $json_response['result']['description'] : '';
        $parameter_errors = [];
        if (!empty($json_response['result']['parameterErrors'])) {
          foreach ($json_response['result']['parameterErrors'] as $parameter_error) {
            $parameter_errors[] = implode(' ', $parameter_error);
          }
        }
        if (!empty($parameter_errors)) {
          $error_description .= ' | parameter errors: ' . implode(' / ', $parameter_errors);
        }
        throw new InvalidResponseException($this->t('Unable to identify OPP payment (requested ID: @checkout_id) - error description: @error_description', [
          '@checkout_id' => $checkout_id,
          '@error_description' => $error_description,
        ]));
      }

      return $this->processTransactionStatus($payment, $json_response);
    }
    catch (RequestException $request_exception) {
      $this->orderLock->release($payment->getOrder());
      throw new InvalidResponseException($this->t('Error occurred on querying OPP checkout status for remote ID @checkout_id with message: @msg', [
        '@checkout_id' => $checkout_id,
        '@msg' => $request_exception->getMessage(),
      ]));
    }
    catch (\Exception $ex) {
      $this->orderLock->release($payment->getOrder());
      throw new InvalidResponseException($this->t('Error occurred on querying OPP checkout status for remote ID @checkout_id with message: @msg', [
        '@checkout_id' => $checkout_id,
        '@msg' => $ex->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processTransactionStatus(PaymentInterface $payment, array $transaction_status) {
    if (!isset($transaction_status['id'], $transaction_status['result']['code'], $transaction_status['result']['description'])) {
      throw new \InvalidArgumentException('CopyAndPayBase::processTransactionStatus() called with a malformed array.');
    }

    if (isset($transaction_status['result']['avsResponse'])) {
      $avs_code = $transaction_status['result']['avsResponse'];
      $payment->setAvsResponseCode($avs_code);
      $avs_codes = $this->getAvsResponseCodeMeanings();
      $payment->setAvsResponseCodeLabel($avs_codes[$avs_code] ?? $this->t('Unkown AVS response code.'));
    }

    $brand_name = $transaction_status['paymentBrand'] ?? NULL;
    $brand = $brand_name ? $this->brandRepository->getBrand($brand_name) : NULL;
    $payment_status = Factory::newInstance($transaction_status['id'], $transaction_status['result']['code'], $transaction_status['result']['description'], $brand);
    if (empty($payment_status)) {
      throw new PaymentGatewayException($this->t('Received unknown payment status @code for checkout ID @remote_id (@description).',
        [
          '@code' => $transaction_status['result']['code'],
          '@remote_id' => $transaction_status['id'],
          '@description' => $transaction_status['result']['description'],
        ]
      ));
    }

    if ($payment_status instanceof SuccessOrPending) {
      if (empty($transaction_status['amount']) || empty($transaction_status['currency'])) {
        $this->logger->warning('Cannot compare paid amount on processing transaction stats, as the amount and/or currency is missing for payment ID @payment_id (status code @status_code).', [
          '@payment_id' => $payment->id(),
          '@status_code' => $payment_status->getCode(),
        ]);
      }
      else {
        $paid_amount = new Price($transaction_status['amount'], $transaction_status['currency']);
        if (!$paid_amount->equals($payment->getAmount())) {
          throw new InvalidResponseException($this->t('The payment amount deviates from the expected value (given: @given_currency @given_amount / expected: @expected_currency @expected_amount).',
            [
              '@given_currency' => $paid_amount->getCurrencyCode(),
              '@given_amount' => $paid_amount->getNumber(),
              '@expected_currency' => $payment->getAmount()->getCurrencyCode(),
              '@expected_amount' => $payment->getAmount()->getNumber(),
            ]
          ));
        }
      }
    }

    $this->alterTransactionStatus($payment_status, $payment, $transaction_status);

    return $payment_status;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentStatusFromSession(PaymentInterface $payment) {
    $session_key = 'opp_status_' . $payment->id();
    $payment_status = NULL;
    if ($payment_status_serialized = $this->privateTempStore->get($session_key)) {
      if (!empty($payment_status_serialized['brand'])) {
        $payment_status_serialized['brand'] = new Brand($payment_status_serialized['brand']);
      }
      $payment_status = Factory::newInstance($payment_status_serialized['id'], $payment_status_serialized['code'], $payment_status_serialized['description'], $payment_status_serialized['brand']);
    }
    return $payment_status;
  }

  /**
   * {@inheritdoc}
   */
  public function savePaymentStatusInSession(PaymentInterface $payment, TransactionStatusInterface $payment_status) {
    $session_key = 'opp_status_' . $payment->id();
    $this->privateTempStore->set($session_key, $payment_status->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionReport(PaymentInterface $payment) {
    $base_url = $payment->getPaymentGatewayMode() == 'live' ? $this->getLiveHostUrl() : $this->getTestHostUrl();
    $url = $base_url . '/v1/query';
    $additional_params = [
      'merchantTransactionId' => $payment->id(),
    ];
    $request_options = $this->getHttpClientRequestOptions(FALSE, $additional_params);
    try {
      $response = $this->httpClient->get($url, $request_options);
      return json_decode($response->getBody(), TRUE);
    }
    catch (RequestException $request_exception) {
      $msg = 'Error occurred on querying OPP transaction report for payment ID @payment_id with message: @msg';
      if ($request_exception->hasResponse()) {
        switch ($request_exception->getResponse()->getStatusCode()) {
          case 404:
            $json_response = json_decode($request_exception->getResponse()->getBody(), TRUE);
            if ($json_response && !empty($json_response['result']['code']) && !empty($json_response['result']['description'])) {
              $msg = 'No OPP transaction report found for payment ID @payment_id, as it is only in pre-authorization state: @msg';
            }
            break;

          case 429:
            $msg = 'Request limit reached on querying OPP transaction report for payment ID @payment_id with message: @msg';
            break;
        }
      }
      throw new InvalidResponseException($this->t($msg, [
        '@payment_id' => $payment->id(),
        '@msg' => $request_exception->getMessage(),
      ]));
    }
    catch (\Exception $ex) {
      throw new InvalidResponseException($this->t('Error occurred on querying OPP transaction report for payment ID @payment_id with message: @msg', [
        '@payment_id' => $payment->id(),
        '@msg' => $ex->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    $resource_path = $request->query->get('resourcePath');
    if (empty($resource_path)) {
      throw new PaymentGatewayException('No resource path found in query string on Open Payment Platform payment return.');
    }
    $checkout_id = $request->query->get('id');
    if (empty($checkout_id)) {
      throw new PaymentGatewayException('No checkout ID specified in query string on Open Payment Platform payment return.');
    }

    $payment = $this->loadPaymentByCheckoutId($checkout_id);
    if (empty($payment)) {
      throw new PaymentGatewayException($this->t('No pre-authorized payment could be found for the checkout ID specified by OPP payment return callback (ID: @checkout_id / resource path: @resource_path)',
        [
          '@checkout_id' => $checkout_id,
          '@resource_path' => $resource_path,
        ])
      );
    }

    if ((int) $payment->getOrderId() !== (int) $order->id()) {
      throw new InvalidResponseException($this->t('The order ID used on the payment return callback (@request_order_id) does not match the parent order ID of the given payment (@payment_order_id). (resource path: @resource_path)',
        [
          '@request_order_id' => $order->id(),
          '@payment_order_id' => $payment->getOrderId(),
          '@resource_path' => $resource_path,
        ]
      ));
    }

    $has_lock = $this->orderLock->lock($order, 5, TRUE);
    if (!$has_lock) {
      $this->logger->warning('Cannot acquire order lock for 5 times. Skip processing payment state. This has to be done via webhooks call - payment ID: @payment_id / order ID: @order_id', [
        '@payment_id' => $payment->id(),
        '@order_id' => $order->id(),
      ]);
      return;
    }

    // We could have an outdated order, just reload it and check the states.
    // @todo Change this when #3043180 is fixed.
    /* @see https://www.drupal.org/project/commerce/issues/3043180 */
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $updated_order = $order_storage->loadUnchanged($order->id());
    $payment = $this->loadPaymentByCheckoutId($checkout_id);

    // If we have different states is because the payment has been validated
    // on the webhooks callback and we need to force the redirection to the next
    // step or it the order will be placed twice.
    if ($payment->isCompleted() || $updated_order->getState()->getId() != $order->getState()->getId()) {
      $this->orderLock->release($order);
      // Get the current checkout step and calculate the next step.
      $step_id = $this->routeMatch->getParameter('step');
      /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
      $checkout_flow = $order->get('checkout_flow')->first()->get('entity')->getTarget()->getValue();
      $checkout_flow_plugin = $checkout_flow->getPlugin();
      $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);

      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $updated_order->id(),
        'step' => $redirect_step_id,
      ])->toString());
    }

    $payment_status = $this->getCheckoutStatus($payment);
    $this->onReturnAction($payment, $payment_status);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);

    $this->cleanupUnusedAuthorizations($order);
  }

  /**
   * Deletes unused authorizations of the same gateway.
   *
   * As we need to create the payment entity already on initializing the
   * payment widget, multiple unused authorizations may be created, if the user
   * steps back and forth or even refreshes the payment checkout page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param int|null $main_payment_id
   *   An optional payment entity ID, that is identified as the main payment
   *   entity. If this argument is set, this payment entity won't be deleted.
   */
  protected function cleanupUnusedAuthorizations(OrderInterface $order, $main_payment_id = NULL) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $this->paymentStorage->loadMultipleByOrder($order);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payments = array_filter($payments, function ($payment) use ($payment_gateway) {
      return $payment->getPaymentGatewayId() == $payment_gateway->id();
    });
    if (!empty($payments)) {
      $payments = array_reverse($payments);
      foreach ($payments as $payment) {
        if ($main_payment_id && $main_payment_id === $payment->id()) {
          continue;
        }
        if ($payment->getState()->getId() == 'authorization') {
          // The payment was cancelled, so let's delete the orphaned entity.
          // Otherwise we could run into problems, that the wrong payment entity
          // is used to print payment instructions for example.
          $payment->delete();
        }
      }
    }
  }

  /**
   * Act onReturn() callback based on the transaction status.
   *
   * This was refactored into a separate function in order to allow single
   * gateway implementations to override standard behaviour (like SIBS MB needs
   * it).
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface $payment_status
   *   The transaction status.
   */
  protected function onReturnAction(PaymentInterface $payment, TransactionStatusInterface $payment_status) {
    if ($payment_status instanceof Pending) {
      $this->logger->info('Skip processing of payment ID @payment_id, as it is in pending state (status code @status_code).', [
        '@payment_id' => $payment->id(),
        '@status_code' => $payment_status->getCode(),
      ]);
      return;
    }

    $payment->setRemoteId($payment_status->getId());
    $payment->setRemoteState($payment_status->getCode());
    if ($payment_status instanceof Success || $payment_status instanceof SuccessNeedingReview) {
      $this->cleanupUnusedAuthorizations($payment->getOrder(), $payment->id());
      $payment->getState()->applyTransitionById('capture');
      $payment->save();
    }
    elseif ($payment_status instanceof Rejected) {
      $payment->getState()->applyTransitionById('void');
      $payment->save();
      throw new PaymentGatewayException($this->t('We could not complete your payment. Please try again or contact us if the problem persists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $base_url = $payment->getPaymentGatewayMode() == 'live' ? $this->getLiveHostUrl() : $this->getTestHostUrl();
    $url = $base_url . '/v1/payments/' . $remote_id;
    $additional_params = [
      'amount' => $amount->getNumber(),
      'currency' => $amount->getCurrencyCode(),
      'paymentType' => 'RF',
    ];
    $request_options = $this->getHttpClientRequestOptions(TRUE, $additional_params);
    try {
      $response = $this->httpClient->post($url, $request_options);
      $json_response = json_decode($response->getBody(), TRUE);
      if (empty($json_response['id'])) {
        throw new InvalidResponseException($this->t('Invalid refund request - response has no ID set.'));
      }

      $brand_name = $json_response['paymentBrand'] ?? NULL;
      $brand = $brand_name ? $this->brandRepository->getBrand($brand_name) : NULL;
      $payment_status = Factory::newInstance($json_response['id'], $json_response['result']['code'], $json_response['result']['description'], $brand);
      if (empty($payment_status)) {
        throw new PaymentGatewayException($this->t('Received unknown payment status @code for refund request of ID @remote_id (@description).',
          [
            '@code' => $json_response['result']['code'],
            '@remote_id' => $remote_id,
            '@description' => $json_response['result']['description'],
          ]
        ));
      }

      if ($payment_status instanceof Rejected) {
        throw new HardDeclineException($this->t('Refund request was rejected: @description',
          [
            '@description' => $json_response['result']['description'],
          ]
        ));
      }
    }
    catch (RequestException $request_exception) {
      throw new InvalidResponseException($this->t('Cannot prepare OPP refund due to exception: @error', ['@error' => $request_exception->getMessage()]));
    }
    catch (\Exception $ex) {
      throw new InvalidResponseException($this->t('Cannot prepare OPP refund due to exception: @error', ['@error' => $ex->getMessage()]));
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getBalance())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveHostUrl() {
    return $this->getMode() == 'test' ? $this->getTestHostUrl() : $this->getLiveHostUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getLiveHostUrl() {
    return !empty($this->configuration['host_live']) ? $this->configuration['host_live'] : CopyAndPayInterface::DEFAULT_HOST_LIVE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTestHostUrl() {
    return !empty($this->configuration['host_test']) ? $this->configuration['host_test'] : CopyAndPayInterface::DEFAULT_HOST_TEST;
  }

  /**
   * {@inheritdoc}
   */
  public function getBrandIds() {
    return $this->configuration['brands'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentTypeParameter() {
    $brands = $this->getBrands();
    // We can only send one paymentType parameter, even if multiple brands are
    // used. However, we only allow credit card gateway configuration to show
    // multiple brands - and these are all debit types anyway. So it's ok to
    // look at the first brand only.
    /** @var \Drupal\commerce_opp\Brand $first_brand */
    $first_brand = reset($brands);
    return $first_brand->getPaymentType();
  }

  /**
   * {@inheritdoc}
   */
  public function getBrands() {
    $brands = $this->getBrandIds();
    array_walk($brands, function (&$value, $key) {
      $value = $this->brandRepository->getBrand($value);
    });
    /** @var \Drupal\commerce_opp\Brand[] $brands */
    return $brands;
  }

  /**
   * {@inheritdoc}
   */
  public function isAmountVisible() {
    return !empty($this->configuration['show_amount']);
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantInvoiceIdFallback(): string {
    return $this->configuration['merchant_invoice_id_fallback'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantTransactionIdField(): string {
    return $this->configuration['merchant_transaction_id_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateCheckoutIdExpireTime($request_time = NULL) {
    if (empty($request_time)) {
      $request_time = $this->time->getRequestTime();
    }
    // A checkout ID is valid for 30 minutes.
    // @see https://docs.oppwa.com/support/widget
    return $request_time + (30 * 60);
  }

  /**
   * {@inheritdoc}
   */
  public function getPayableAmount(OrderInterface $order) {
    $event = new AlterPaymentAmountEvent($order);
    $this->eventDispatcher->dispatch($event, OpenPaymentPlatformPaymentEvents::ALTER_AMOUNT);
    $payment_amount = $event->getPaymentAmount();
    return $this->rounder->round($payment_amount);
  }

  /**
   * {@inheritdoc}
   */
  public function loadPaymentByCheckoutId($opp_checkout_id) {
    $query = $this->paymentStorage->getQuery();
    $query->condition('opp_checkout_id', $opp_checkout_id);
    $query->accessCheck(FALSE);
    $payment_ids = $query->execute();
    if (empty($payment_ids)) {
      return NULL;
    }
    $payment_id = reset($payment_ids);
    return $this->paymentStorage->loadUnchanged($payment_id);
  }

  /**
   * Returns an array with request options ready to use for http client.
   *
   * This function will set the authentication options correctly and respects
   * the optionally passed parameters, will be either set as query string
   * parameters, if a GET request is prepared, or as form parameters, if a POST
   * request is being prepared.
   *
   * @param bool $prepare_post_request
   *   TRUE, if a POST request is being prepared. FALSE, if it's a GET request.
   *   Defaults to TRUE.
   * @param string[] $additional_params
   *   Additional request parameters. Depending if a GET or a POST request is
   *   being prepared, the params will be set as query string or form params.
   *
   * @return array
   *   An array containing the authentication data, ready to be used with Guzzle
   *   http client as $options parameter in its get/post functions.
   */
  protected function getHttpClientRequestOptions($prepare_post_request = TRUE, array $additional_params = []) {
    $options = [
      RequestOptions::HEADERS => [
        'Authorization' => 'Bearer ' . $this->configuration['access_token'],
      ],
    ];
    $params = [
      'entityId' => $this->configuration['entity_id'],
    ];
    if (!empty($additional_params)) {
      $params += $additional_params;
    }
    $options[$prepare_post_request ? RequestOptions::FORM_PARAMS : RequestOptions::QUERY] = $params;
    return $options;
  }

  /**
   * Returns whether multiple brands are allowed within a single instance.
   *
   * Only credit cards are subject to be allowed to have multiple brands
   * configured in a single gateway because they are the only ones that can be
   * rendered in a single PAYFRAME widget. For every other payment type,
   * multiple PAYFRAME widgets would be rendered one after another on the
   * payment page, which is very confusing. It is better to select the payment
   * method first in the checkout step and have a single widget rendered on the
   * payment page then.
   *
   * @return bool
   *   TRUE, if the multiple brands are allowed to be configured within a single
   *   gateway instance, FALSE otherwise.
   */
  protected function allowMultipleBrands() {
    return FALSE;
  }

  /**
   * Gives gateway plugins possibility to hook into processTransactionStatus().
   *
   * @param \Drupal\commerce_opp\Transaction\Status\TransactionStatusInterface $transaction_status
   *   The transaction status.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param array $json_response
   *   The JSON response from the payment status request.
   */
  protected function alterTransactionStatus(TransactionStatusInterface $transaction_status, PaymentInterface $payment, array $json_response) {
  }

  /**
   * Returns allowed brand options suitable for select list in config form.
   *
   * @return array
   *   The brands labels, keyed by brand ID.
   */
  abstract protected function getBrandOptions();

  /**
   * Gets all available AVS response code meanings.
   *
   * @return array
   *   The AVS response code meanings.
   */
  protected function getAvsResponseCodeMeanings() {
    return [
      'A' => $this->t('Address does match, zip code does not match'),
      'Z' => $this->t('Address does not match, zip code does match'),
      'N' => $this->t('Address and zip code do not match'),
      'U' => $this->t('Technical or logical error. AVS cannot be applied on card or address (not UK or US issuer), issuer is not available, etc.'),
      'F' => $this->t('Address and Postal Code Matches'),
    ];
  }

}
