<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a dedicated PAYFRAME gateway for MBWAY virtual account.
 *
 * @CommercePaymentGateway(
 *   id = "opp_copyandpay_mbway",
 *   label = "Open Payment Platform PAYFRAME: MBWAY",
 *   display_label = "MBWAY",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_opp\PluginForm\CopyAndPayMbwayForm",
 *   },
 *   payment_type = "opp",
 *   requires_billing_information = FALSE,
 * )
 */
class CopyAndPayMbway extends CopyAndPayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'test_mode' => 'EXTERNAL',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['brands']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $this->configuration['brands'] = ['MBWAY'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCheckout(array $params = [], PaymentInterface $payment = NULL) {
    if ($this->getMode() == 'test') {
      $params['customParameters[SIBS_ENV]'] = 'QLY';
    }

    return parent::prepareCheckout($params, $payment);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateCheckoutIdExpireTime($request_time = NULL) {
    if (empty($request_time)) {
      $request_time = $this->time->getRequestTime();
    }
    // A checkout ID for MB WAY is only valid for 4 minutes.
    return $request_time + (4 * 60);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBrandOptions() {
    $brand = $this->brandRepository->getBrand('MBWAY');
    return [$brand->getId() => $brand->getLabel()];
  }

}
