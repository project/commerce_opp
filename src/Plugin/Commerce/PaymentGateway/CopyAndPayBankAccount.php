<?php

namespace Drupal\commerce_opp\Plugin\Commerce\PaymentGateway;

/**
 * Provides the Open Payment Platform PAYFRAME gateway for bank accounts.
 *
 * @CommercePaymentGateway(
 *   id = "opp_copyandpay_bank",
 *   label = "Open Payment Platform PAYFRAME (bank transfer)",
 *   display_label = "Bank transfer",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_opp\PluginForm\CopyAndPayForm",
 *   },
 *   payment_type = "opp",
 *   requires_billing_information = FALSE,
 * )
 */
class CopyAndPayBankAccount extends CopyAndPayBase {

  /**
   * {@inheritdoc}
   */
  protected function getBrandOptions() {
    return $this->brandRepository->getBankAccountBrandLabels();
  }

}
