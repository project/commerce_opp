<?php

namespace Drupal\commerce_opp\Controller;

use Drupal\commerce_opp\OrderLockInterface;
use Drupal\commerce_opp\Transaction\Status\Pending;
use Drupal\commerce_opp\Transaction\Status\Rejected;
use Drupal\commerce_opp\Transaction\Status\Success;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Provides the webhooks controller, providing the webhooks endpoint.
 */
class WebhooksController extends ControllerBase {

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The order lock service.
   *
   * @var \Drupal\commerce_opp\OrderLockInterface
   */
  protected $orderLock;

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * Constructs a new WebhooksController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\commerce_opp\OrderLockInterface $order_lock
   *   The order lock service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, OrderLockInterface $order_lock) {
    $this->config = $config_factory->get('commerce_opp.settings');
    $this->orderLock = $order_lock;
    $this->paymentStorage = $this->entityTypeManager()->getStorage('commerce_payment');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('commerce_opp.order_lock')
    );
  }

  /**
   * The webhooks endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   */
  public function endpoint(Request $request) {
    $init_vector = $request->headers->get('X-Initialization-Vector');
    $auth_tag = $request->headers->get('X-Authentication-Tag');
    if (empty($init_vector) || empty($auth_tag)) {
      throw new AccessDeniedHttpException($this->t('Web hook calls must include an authentication tag and initialization vector!'));
    }
    $encryption_secret = $this->config->get('encryption_secret');
    if (empty($encryption_secret)) {
      throw new BadRequestHttpException($this->t('No encryption secret is configured. Unable to decrypt message.'));
    }

    $notification = $this->decrypt($request->getContent(), $init_vector, $auth_tag, $encryption_secret);
    if (empty($notification)) {
      throw new BadRequestHttpException($this->t('Bad request - could not decrypt message properly or invalid request body.'));
    }

    $payload = $notification['payload'];
    $notification_type = mb_strtoupper($notification['type']);
    switch ($notification_type) {
      case 'PAYMENT':
        $remote_id = $payload['id'];
        if (empty($remote_id)) {
          throw new BadRequestHttpException($this->t('PAYMENT notifications must have the payment ID set.'));
        }

        // Restrict the payment types to debit, capture and receipt of payments.
        $allowed_payment_types = ['DB', 'CP', 'RC'];
        if (!in_array($payload['paymentType'], $allowed_payment_types)) {
          throw new BadRequestHttpException($this->t('Only payments of types DB, CP and RC are processed via webhooks. Given @payment_type for remote ID @remote_id.', [
            '@payment_type' => $payload['paymentType'],
            '@remote_id' => $remote_id,
          ]));
        }

        $payment = $this->paymentStorage->loadByRemoteId($remote_id);
        if (empty($payment)) {
          // As a fallback, try to load the payment by checkout ID instead.
          $payment = $this->loadPaymentByCheckoutId($payload['ndc']);
        }
        if (empty($payment)) {
          throw new BadRequestHttpException($this->t('No payment entity found with remote ID @remote_id.', [
            '@remote_id' => $remote_id,
          ]));
        }

        if ($payment->isCompleted()) {
          // Early exit on already completed orders.
          $this->getLogger('commerce_opp')->info('Skip webhook processing of payment ID @payment_id, as it has been already completed.', [
            '@payment_id' => $payment->id(),
          ]);

          return new JsonResponse([
            'processed' => FALSE,
          ]);
        }

        /** @var \Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface $gateway */
        $gateway = $payment->getPaymentGateway()->getPlugin();
        $transaction_status = $gateway->processTransactionStatus($payment, $payload);
        if ($transaction_status instanceof Pending) {
          $this->getLogger('commerce_opp')->info('Skip webhook processing of payment ID @payment_id, as it is in pending state (status code @status_code).', [
            '@payment_id' => $payment->id(),
            '@status_code' => $transaction_status->getCode(),
          ]);
        }
        else {
          $has_lock = $this->orderLock->lock($payment->getOrder());
          if (!$has_lock) {
            throw new HttpException(425, 'Cannot acquire lock for given order - try again later!');
          }

          $success = $transaction_status instanceof Success || $transaction_status instanceof SuccessNeedingReview;
          if ($success) {
            $payment->getState()->applyTransitionById('capture');
            if (empty($payment->getRemoteId())) {
              $payment->setRemoteId($remote_id);
            }
            $payment->setRemoteState($transaction_status->getCode());
            $payment->save();
          }
          elseif ($transaction_status instanceof Rejected) {
            $payment->getState()->applyTransitionById('void');
            if (empty($payment->getRemoteId())) {
              $payment->setRemoteId($remote_id);
            }
            $payment->setRemoteState($transaction_status->getCode());
            $payment->save();
          }
          $this->getLogger('commerce_opp')->info('Processed webhook notification for remote payment ID @remote_id. Code: @code -> successful payment: @success', [
            '@remote_id' => $remote_id,
            '@code' => $transaction_status->getCode(),
            '@success' => $success ? $this->t('yes') : $this->t('no'),
          ]);
        }
        break;

      case 'TEST':
        $this->getLogger('commerce_opp')->debug('Received webhooks TEST notification.');
        break;

      case 'REGISTRATION':
      case 'RISK':
      default:
        throw new BadRequestHttpException($this->t('Invalid notification type "@notification_type". Only "PAYMENT" AND "TEST" type notifications are supported.', [
          '@notification_type' => $notification_type,
        ]));
    }

    return new JsonResponse([
      'processed' => TRUE,
    ]);
  }

  /**
   * Decrypts the message.
   *
   * @param string $encrypted_message
   *   The encrypted message.
   * @param string $initialization_vector
   *   The initialization vector (as given in HTTP header).
   * @param string $authentication_tag
   *   The authentication tag (as given in HTTP header).
   * @param string $encryption_secret
   *   The encryption secret (as configured).
   *
   * @return array
   *   The decrypted message as JSON array.
   */
  protected function decrypt(string $encrypted_message, string $initialization_vector, string $authentication_tag, string $encryption_secret): array {
    $key = hex2bin($encryption_secret);
    $iv = hex2bin($initialization_vector);
    $auth_tag = hex2bin($authentication_tag);
    $cipher_text = hex2bin($encrypted_message);
    $msg = openssl_decrypt($cipher_text, "aes-256-gcm", $key, OPENSSL_RAW_DATA, $iv, $auth_tag);
    return $msg ? json_decode($msg, TRUE) : [];
  }

  /**
   * Loads a payment by its OPP checkout ID.
   *
   * @param string $opp_checkout_id
   *   The OPP checkout ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity, if found.
   */
  protected function loadPaymentByCheckoutId(string $opp_checkout_id) {
    $query = $this->paymentStorage->getQuery();
    $query->condition('opp_checkout_id', $opp_checkout_id);
    $query->accessCheck(FALSE);
    $payment_ids = $query->execute();
    if (empty($payment_ids)) {
      return NULL;
    }
    $payment_id = reset($payment_ids);
    return $this->paymentStorage->load($payment_id);
  }

}
