<?php

namespace Drupal\commerce_opp\Controller;

use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_opp\BrandRepositoryInterface;
use Drupal\commerce_opp\Plugin\Commerce\PaymentType\Opp;
use Drupal\commerce_opp\Transaction\Status\Factory;
use Drupal\commerce_opp\Transaction\Status\Rejected;
use Drupal\commerce_opp\Transaction\Status\Success;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the transaction controller, providing extra route for status check.
 */
class TransactionController extends ControllerBase {

  /**
   * The brand repository.
   *
   * @var \Drupal\commerce_opp\BrandRepositoryInterface
   */
  protected $brandRepository;

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * Constructs a new TransactionController object.
   *
   * @param \Drupal\commerce_opp\BrandRepositoryInterface $brand_repository
   *   The brand repository.
   * @param \Drupal\commerce_cart\CartSessionInterface $cart_session
   *   The cart session.
   */
  public function __construct(BrandRepositoryInterface $brand_repository, CartSessionInterface $cart_session) {
    $this->brandRepository = $brand_repository;
    $this->cartSession = $cart_session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_opp.brand_repository'),
      $container->get('commerce_cart.cart_session')
    );
  }

  /**
   * Checks the transaction status of a given payment.
   *
   * This is only needed for certain brands, currently only MB WAY, where the
   * customer has a few minutes time to pay externally (via mobile app eg.).
   * We need an external route, where we wait for the payment, checking the
   * status every 30 seconds.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   *   The payment entity.
   * @param string $type
   *   The OPP payment type to check. Defaults to 'DB'.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|array
   *   Either a JSON response for Ajax requests, or a render array or redirect
   *   response for static requests.
   */
  public function checkStatus(Request $request, PaymentInterface $commerce_payment, $type = 'DB') {
    /** @var \Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface $gateway */
    $gateway = $commerce_payment->getPaymentGateway()->getPlugin();
    $is_ajax = $request->isXmlHttpRequest();

    $cancel_url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $commerce_payment->getOrderId(),
      'step' => 'order_information',
    ]);

    $success_url = Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $commerce_payment->getOrderId(),
      'step' => 'payment',
    ], [
      'query' => $request->query->all(),
    ]);

    $payment_status = $gateway->getPaymentStatusFromSession($commerce_payment);
    if (empty($payment_status)) {
      try {
        $transaction_report = $gateway->getTransactionReport($commerce_payment);
        $payment_info = NULL;
        foreach ($transaction_report['payments'] as $single_payment_info) {
          if ($single_payment_info['paymentType'] != $type) {
            continue;
          }
          $payment_info = $single_payment_info;
          break;
        }

        if (!empty($payment_info)) {
          $brand = $this->brandRepository->getBrand($payment_info['paymentBrand']);
          $payment_status = Factory::newInstance($payment_info['id'], $payment_info['result']['code'], $payment_info['result']['description'], $brand);
        }
      }
      catch (InvalidRequestException $ex) {
        // This is typically a "Too Many Requests" exception.
        $this->getLogger('commerce_opp')->warning($ex->getMessage());
        if ($is_ajax) {
          return new JsonResponse([
            'status' => 'pending',
          ]);
        }
        else {
          return [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('Please wait, while we are checking the transaction status in the background. You will be automatically redirected on success or error.'),
            '#attached' => [
              'library' => [
                'commerce_opp/check_transaction_status',
              ],
              'drupalSettings' => [
                'commerce_opp' => [
                  'check_status_url' => Url::fromRoute('commerce_opp.check_transaction_status', [
                    'commerce_payment' => $commerce_payment->id(),
                  ],
                    ['absolute' => TRUE])->toString(),
                ],
              ],
            ],
          ];
        }
      }
    }

    if (empty($payment_status)) {
      if ($is_ajax) {
        return new JsonResponse([
          'status' => 'error',
        ]);
      }
      else {
        $this->messenger()->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
        return $this->buildRedirectResponse($cancel_url, $commerce_payment);
      }
    }

    if ($payment_status instanceof Success) {
      if ($commerce_payment->getState()->getId() == 'authorization') {
        $gateway->savePaymentStatusInSession($commerce_payment, $payment_status);
        $commerce_payment->setRemoteId($payment_status->getId());
        $commerce_payment->setRemoteState($payment_status->getCode());
        $commerce_payment->getState()->applyTransitionById('capture');
        $commerce_payment->save();
      }
      if ($is_ajax) {
        return new JsonResponse([
          'status' => 'success',
        ]);
      }
      else {
        return $this->buildRedirectResponse($success_url, $commerce_payment);
      }
    }
    elseif ($payment_status instanceof Rejected) {
      if ($commerce_payment->getState()->getId() == 'authorization') {
        $commerce_payment->setRemoteId($payment_status->getId());
        $commerce_payment->setRemoteState($payment_status->getCode());
        $commerce_payment->getState()->applyTransitionById('void');
        $commerce_payment->save();
      }
      if ($is_ajax) {
        return new JsonResponse([
          'status' => 'rejected',
        ]);
      }
      else {
        $this->messenger()->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
        return $this->buildRedirectResponse($cancel_url, $commerce_payment);
      }
    }
    else {
      if ($is_ajax) {
        return new JsonResponse([
          'status' => 'pending',
        ]);
      }
      else {
        return [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Please wait, while we are checking the transaction status in the background. You will be automatically redirected on success or error.'),
          '#attached' => [
            'library' => [
              'commerce_opp/check_transaction_status',
            ],
            'drupalSettings' => [
              'commerce_opp' => [
                'check_status_url' => Url::fromRoute('commerce_opp.check_transaction_status', [
                  'commerce_payment' => $commerce_payment->id(),
                ], ['absolute' => TRUE])->toString(),
              ],
            ],
          ],
        ];
      }
    }
  }

  /**
   * Checks access for the transaction status page.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   *   The payment entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(PaymentInterface $commerce_payment, AccountInterface $account) {
    if (!($commerce_payment->getType() instanceof Opp)) {
      return AccessResult::forbidden()->addCacheableDependency($commerce_payment);
    }

    $order = $commerce_payment->getOrder();
    if (empty($order)) {
      // Invalid data - payment without order.
      return AccessResult::forbidden()->addCacheableDependency($commerce_payment);
    }

    // Check for cart ownership.
    if ($account->isAuthenticated()) {
      $customer_check = $account->id() == $order->getCustomerId();
    }
    else {
      $active_cart = $this->cartSession->hasCartId($order->id(), CartSessionInterface::ACTIVE);
      $completed_cart = $this->cartSession->hasCartId($order->id(), CartSessionInterface::COMPLETED);
      $customer_check = $active_cart || $completed_cart;
    }

    return AccessResult::allowedIf($customer_check)
      ->andIf(AccessResult::allowedIfHasPermission($account, 'access checkout'))
      ->addCacheableDependency($commerce_payment);
  }

  /**
   * Builds a redirect response to the target url, incl cache metadata.
   *
   * @param \Drupal\Core\Url $target_url
   *   The redirect target url.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity - needed for collecting cache metadata.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  protected function buildRedirectResponse(Url $target_url, PaymentInterface $payment) {
    /*
     * @todo this fixes the WSOD temporarily.
     * We need to try wrapping the payment manipulating code inside:
     * $context = new RenderContext();
     * $response = $this->renderer->executeInRenderContext(...) {...}
     */
    return new RedirectResponse($target_url->toString());
    $response = new LocalRedirectResponse($target_url->toString());
    $response->addCacheableDependency($payment);
    $response->addCacheableDependency($payment->getOrder());
    $response->addCacheableDependency($payment->getOrder()->getCustomer());
    $cache = new CacheableMetadata();
    $cache->setCacheMaxAge(0);
    $cache->setCacheContexts(['session']);
    $response->addCacheableDependency($cache);
    return $response;
  }

}
