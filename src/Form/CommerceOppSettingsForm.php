<?php

namespace Drupal\commerce_opp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides global settings for commerce_opp module.
 */
class CommerceOppSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_opp_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_opp.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_opp.settings');

    $form['encryption_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhooks encryption secret'),
      '#default_value' => $config->get('encryption_secret'),
      '#description' => $this->t('The webhooks encryption secret. Only needed, if you are going to use webhooks. See the README.md file for more information on webhooks.'),
      '#size' => 78,
    ];

    $form['cron_expiration_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Cron expiration threshold'),
      '#description' => $this->t('Defines additional seconds for how long the cron job will try to process pending authorizations after its official expiration on the on side, and not delete expired authorizations on cron on the other side. Defaults to 86400 for one additional day.'),
      '#default_value' => $config->get('cron_expiration_threshold'),
      '#min' => 0,
      '#max' => 2592000,
      '#step' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('commerce_opp.settings');
    $config
      ->set('encryption_secret', $form_state->getValue('encryption_secret'))
      ->set('cron_expiration_threshold', $form_state->getValue('cron_expiration_threshold'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
