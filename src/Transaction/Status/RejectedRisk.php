<?php

namespace Drupal\commerce_opp\Transaction\Status;

/**
 * Base class for rejected status due to all kind of risk checks.
 */
abstract class RejectedRisk extends Rejected {

}
