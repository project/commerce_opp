<?php

namespace Drupal\commerce_opp\Transaction\Status;

/**
 * Abstract base class for all rejected status types.
 */
abstract class Rejected extends AbstractStatus {

}
