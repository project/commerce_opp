<?php

namespace Drupal\commerce_opp\Transaction\Status;

/**
 * Abstract base class for all success and pending status types.
 */
abstract class SuccessOrPending extends AbstractStatus {

}
