<?php

namespace Drupal\commerce_opp\Commands;

use Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Payment related Drush commands.
 */
class PaymentCommands extends DrushCommands {

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * Constructs a new PaymentCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
  }

  /**
   * Queries and prints the transaction status of an OPP payment.
   *
   * @param int $payment_id
   *   The payment ID.
   * @param array $options
   *   The Drush command options.
   *
   * @command commerce_opp:transaction-status
   *
   * @option type whether to query transaction report or checkout status.
   *   Defaults to 'report'. Use 'checkout' to use checkout status instead.
   *
   * @usage commerce_opp:transaction-status
   *   Queries the transaction status of an OPP payment.
   *
   * @aliases opp:ts
   *
   * @throws \InvalidArgumentException
   *   If the parameter is invalid.
   */
  public function printTransactionStatus($payment_id, array $options = ['type' => 'report']) {
    if (!is_numeric($payment_id) || $payment_id < 1) {
      throw new \InvalidArgumentException('You must specify a valid positive integer.');
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->paymentStorage->load($payment_id);
    if (empty($payment)) {
      throw new \InvalidArgumentException(sprintf('%s is an invalid payment ID.', $payment_id));
    }
    $order = $payment->getOrder();
    if (empty($order)) {
      throw new \InvalidArgumentException(sprintf('There is no order attached to payment ID %s.', $payment_id));
    }

    $gateway = $payment->getPaymentGateway()->getPlugin();
    if (!($gateway instanceof CopyAndPayInterface)) {
      throw new \InvalidArgumentException(sprintf('%s is not an OPP payment.', $payment_id));
    }
    /** @var \Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface $gateway */
    $use_checkout_status = !empty($options['type']) && $options['type'] == 'checkout';
    $transaction_status = $use_checkout_status ? $gateway->getCheckoutStatus($payment) : $gateway->getTransactionReport($payment);
    $this->output()->writeln(print_r($transaction_status, TRUE));
  }

}
