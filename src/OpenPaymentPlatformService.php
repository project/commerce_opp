<?php

namespace Drupal\commerce_opp;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Default Open Payment Platform service implementation.
 */
class OpenPaymentPlatformService implements OpenPaymentPlatformServiceInterface {

  use StringTranslationTrait;

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * The payment gateway storage.
   *
   * @var \Drupal\commerce_payment\PaymentGatewayStorageInterface
   */
  protected $paymentGatewayStorage;

  /**
   * The 'state' key/value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new OpenPaymentPlatformService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactory $key_value_factory
   *   The key/value store factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, KeyValueFactory $key_value_factory, LoggerChannelFactoryInterface $logger_channel_factory, ConfigFactoryInterface $config_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->paymentGatewayStorage = $entity_type_manager->getStorage('commerce_payment_gateway');
    $this->keyValue = $key_value_factory->get('state');
    $this->logger = $logger_channel_factory->get('commerce_opp');
    $this->config = $config_factory->get('commerce_opp.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getOppGatewayIds(bool $only_active = TRUE, array $config_filter = [], array $plugins = []): array {
    $query = $this->paymentGatewayStorage->getQuery();
    $opp_plugin_ids = [
      'opp_copyandpay_bank',
      'opp_copyandpay_card',
      'opp_copyandpay_virtual',
      'opp_copyandpay_mbway',
      'opp_copyandpay_sibs_multibanco',
      'opp_copyandpay_sofortueberweisung',
    ];
    $query->condition('plugin', $opp_plugin_ids, 'IN');
    if ($only_active) {
      $query->condition('status', TRUE);
    }
    foreach ($config_filter as $key => $value) {
      $query->condition('configuration.' . $key, $value);
    }
    if (!empty($plugins)) {
      $query->condition('plugin', $plugins, 'IN');
    }
    $query->accessCheck(FALSE);
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteExpiredAuthorizations() {
    $opp_gateway_ids = $this->getOppGatewayIds(TRUE, [
      'cron_delete_expired_authorizations' => TRUE,
    ]);

    if (empty($opp_gateway_ids)) {
      return;
    }

    $query = $this->paymentStorage->getQuery();
    $query->condition('payment_gateway', $opp_gateway_ids, 'IN');
    $query->condition('state', 'authorization');
    $query->condition('expires', $this->getExpiresThreshold(), '<');
    $query->condition('type', 'opp_sibs_multibanco', '<>');
    $query->accessCheck(FALSE);
    $payments = $query->execute();
    if (empty($payments)) {
      return;
    }
    $payments = $this->paymentStorage->loadMultiple($payments);
    $this->paymentStorage->delete($payments);
  }

  /**
   * {@inheritdoc}
   */
  public function processPendingAuthorizations() {
    $opp_gateway_ids = $this->getOppGatewayIds(FALSE, [
      'cron_process_pending_authorizations' => TRUE,
    ]);

    if (empty($opp_gateway_ids)) {
      return;
    }

    $query = $this->paymentStorage->getQuery();
    $query->condition('payment_gateway', $opp_gateway_ids, 'IN');
    $query->condition('state', 'authorization');
    $query->condition('expires', $this->getExpiresThreshold(), '>=');
    $query->accessCheck(FALSE);
    $payments = $query->execute();
    if (empty($payments)) {
      return;
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $this->paymentStorage->loadMultiple($payments);
    foreach ($payments as $payment) {
      /** @var \Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface $gateway */
      $gateway = $payment->getPaymentGateway()->getPlugin();
      $gateway->checkAndUpdateTransactionStatus($payment);
    }
  }

  /**
   * Calculates the expiration threshold for the cron tasks.
   *
   * @return int
   *   The threshold as unix timestamp used as query condition for the expires
   *   timestamp of payment entities on both cron tasks for deleting expired
   *   authorizations and process pending authorizations. The result is based
   *   on the last system cron run, as well as on the configurable extra cron
   *   expiration threshold.
   */
  protected function getExpiresThreshold() {
    $additional_lifetime = $this->config->get('cron_expiration_threshold');
    $expires_threshold = (int) $this->keyValue->get('system.cron_last');
    if ($additional_lifetime > 0) {
      $expires_threshold -= $additional_lifetime;
    }
    return $expires_threshold;
  }

}
