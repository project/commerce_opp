<?php

namespace Drupal\commerce_opp\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the interface for shipping profile resolvers.
 */
interface ShippingProfileResolverInterface {

  /**
   * Resolves the shipping profile.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The profile entity, if resolved. Otherwise NULL, indicating that the next
   *   resolver in the chain should be called.
   */
  public function resolve(OrderInterface $order);

}
