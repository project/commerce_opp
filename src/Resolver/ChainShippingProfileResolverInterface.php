<?php

namespace Drupal\commerce_opp\Resolver;

/**
 * Runs the added resolvers one by one, until the shipping profile is returned.
 *
 * Each resolver in the chain can be another chain, which is why this interface
 * extends the shipping profile resolver one.
 */
interface ChainShippingProfileResolverInterface extends ShippingProfileResolverInterface {

  /**
   * Adds a resolver.
   *
   * @param \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface $resolver
   *   The resolver.
   */
  public function addResolver(ShippingProfileResolverInterface $resolver);

  /**
   * Gets all added resolvers.
   *
   * @return \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface[]
   *   The resolvers.
   */
  public function getResolvers();

}
