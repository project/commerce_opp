<?php

namespace Drupal\commerce_opp\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Returns the shipping profile of a given order entity.
 *
 * This way, we can provide shipping address data even, if not commerce_shipping
 * is used but a custom solution for example.
 */
class DefaultShippingProfileResolver implements ShippingProfileResolverInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new DefaultShippingProfileResolver object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order) {
    if ($this->moduleHandler->moduleExists('commerce_shipping') && $order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
        return $shipment->getShippingProfile();
      }
    }

    return NULL;
  }

}
