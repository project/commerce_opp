<?php

namespace Drupal\commerce_opp\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Default implementation of the chain shipping profile resolver.
 */
class ChainShippingProfileResolver implements ChainShippingProfileResolverInterface {

  /**
   * The resolvers.
   *
   * @var \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Constructs a new ChainShippingProfileResolver object.
   *
   * @param \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface[] $resolvers
   *   The resolvers.
   */
  public function __construct(array $resolvers = []) {
    $this->resolvers = $resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function addResolver(ShippingProfileResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolvers() {
    return $this->resolvers;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order) {
    foreach ($this->resolvers as $resolver) {
      $result = $resolver->resolve($order);
      if ($result) {
        return $result;
      }
    }
  }

}
