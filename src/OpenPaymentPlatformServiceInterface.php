<?php

namespace Drupal\commerce_opp;

/**
 * Defines the Open Payment Platform service interface.
 */
interface OpenPaymentPlatformServiceInterface {

  /**
   * Returns a list of configured Open Payment Platform gateway IDs.
   *
   * @param bool $only_active
   *   Whether to only return active gateways. Defaults to TRUE.
   * @param array $config_filter
   *   An optional array of key-value pairs of configuration values to filter.
   *   Do not use the 'configuration.' prefix, as it will added automatically.
   *   Example: to filter for gateways with 'cron_delete_expired_authorizations'
   *   enabled, use this: ['cron_delete_expired_authorizations' => TRUE].
   * @param string[] $plugins
   *   Restrict to certain plugin IDs. If left empty, all plugin IDs will be
   *   searched.
   *
   * @return string[]
   *   An array of configured Open Payment Platform gateway IDs.
   */
  public function getOppGatewayIds(bool $only_active = TRUE, array $config_filter = [], array $plugins = []): array;

  /**
   * Deletes expired Open Payment Platform authorizations.
   *
   * Every time, an Open Payment Platform plugin is selected on checkout and the
   * payment page is visited then, a new payment entity with state
   * "authorization" is created and saved. If the customer does not complete the
   * payment, or is uncertain and jumps back and forth in the checkout process,
   * a lot of unneeded data will be generated and stored then. This service does
   * the cleanup work and deletes expired Open Payment Platform authorizations.
   */
  public function deleteExpiredAuthorizations();

  /**
   * Processes pending authorizations.
   *
   * Open Payment Platform does not actively send notification callbacks as many
   * other gateways do. Instead they only send the checkout ID alongside calling
   * the return URL. This means, if for whatever reason the customer doesn't get
   * redirected back to the payment return page, we'll never get notified about
   * the transaction status, leading to unplaced and locked orders, although the
   * payment was most likely successful.
   *
   * Therefore we need to proactively fetch the transaction status for every
   * pending authorization on cron run by ourselves.
   *
   * To be precise, server-to-server notifications do exist, but they need to be
   * configured separately (and its configuration possibility in the payment
   * gateway's administration is often hidden by default for customers). These
   * notifications are called 'webhooks'. Please read the shipped README.md file
   * for more info on this topic, including configuration guidance.
   *
   * In the configuration of your commerce_opp gateway plugins, you'll find a
   * setting on whether or not to process pending authorizations. Disabling this
   * makes sense, if you have webhooks configured and verified that the webhooks
   * are working. Without webhooks, it is strongly recommended to keep this
   * setting enabled, as you might miss out orders that are actually paid.
   */
  public function processPendingAuthorizations();

}
