<?php

namespace Drupal\commerce_opp\EventSubscriber;

use Drupal\commerce_opp\OrderLockInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber releasing active order locks on terminating the request.
 */
class OrderLockReleaseEventSubscriber implements EventSubscriberInterface {

  /**
   * The order lock service.
   *
   * @var \Drupal\commerce_opp\OrderLockInterface
   */
  protected $orderLock;

  /**
   * Constructs a new OrderLockReleaseEventSubscriber object.
   *
   * @param \Drupal\commerce_opp\OrderLockInterface $order_lock
   *   The order lock service.
   */
  public function __construct(OrderLockInterface $order_lock) {
    $this->orderLock = $order_lock;
  }

  /**
   * Releases open order locks once output flushed.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The post response event.
   */
  public function releaseOrderLocks(TerminateEvent $event) {
    $this->orderLock->releaseAll();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::TERMINATE => 'releaseOrderLocks',
    ];
  }

}
