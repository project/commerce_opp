<?php

namespace Drupal\commerce_opp\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the PAYFRAME plugin form for the MB WAY brand.
 */
class CopyAndPayMbwayForm extends CopyAndPayForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $form['cards']['#action'] = Url::fromRoute('commerce_opp.check_transaction_status', [
      'commerce_payment' => $payment->id(),
    ], ['absolute' => TRUE])->toString();

    return $form;
  }

}
