<?php

namespace Drupal\commerce_opp\PluginForm;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use CommerceGuys\Intl\Formatter\NumberFormatterInterface;
use Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPaySofortueberweisungInterface;
use Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPaySibsMultibanco;
use Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the PAYFRAME plugin form.
 */
class CopyAndPayForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The number formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\NumberFormatterInterface
   */
  protected $numberFormatter;

  /**
   * The (chain) shipping profile resolver.
   *
   * @var \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface
   */
  protected $shippingProfileResolver;

  /**
   * Constructs a new CopyAndPayForm object.
   *
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \CommerceGuys\Intl\Formatter\NumberFormatterInterface $number_formatter
   *   The number formatter.
   * @param \Drupal\commerce_opp\Resolver\ShippingProfileResolverInterface $shipping_profile_resolver
   *   The shipping profile resolver.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(CurrencyFormatterInterface $currency_formatter, LanguageManagerInterface $language_manager, NumberFormatterInterface $number_formatter, ShippingProfileResolverInterface $shipping_profile_resolver) {
    $this->currencyFormatter = $currency_formatter;
    $this->languageManager = $language_manager;
    $this->numberFormatter = $number_formatter;
    $this->shippingProfileResolver = $shipping_profile_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_price.currency_formatter'),
      $container->get('language_manager'),
      $container->get('commerce_price.number_formatter'),
      $container->get('commerce_opp.chain_shipping_profile_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_opp\Plugin\Commerce\PaymentGateway\CopyAndPayInterface $opp_gateway */
    $opp_gateway = $payment->getPaymentGateway()->getPlugin();
    $brand_ids = $opp_gateway->getBrandIds();

    $payment_amount = $opp_gateway->getPayableAmount($order);
    $payment->setAmount($payment_amount);
    // Save to get an ID.
    $payment->save();

    $request_params = [
      'currency' => $payment_amount->getCurrencyCode(),
      'amount' => $this->numberFormatter->format($payment_amount->getNumber(), [
        'locale' => 'en',
        'use_grouping' => FALSE,
        'minimum_fraction_digits' => 2,
        'maximum_fraction_digits' => 2,
      ]),
      'paymentType' => $opp_gateway->getPaymentTypeParameter(),
      'descriptor' => $order->getStore()->label(),
      'customer.email' => $order->getEmail(),
      'customer.ip' => $order->getIpAddress(),
      'merchantInvoiceId' => $order->getOrderNumber() ?: ($opp_gateway->getMerchantInvoiceIdFallback() === 'uuid' ? $order->uuid() : $order->id()),
      'merchantTransactionId' => $opp_gateway->getMerchantTransactionIdField() === 'uuid' ? $payment->uuid() : $payment->id(),
    ];

    $customer = $order->getCustomer();
    if ($customer && $customer->isAuthenticated()) {
      $request_params['customer.merchantCustomerId'] = $customer->id();
    }

    $billing_profile = $order->getBillingProfile();
    /** @var \Drupal\address\AddressInterface|null $billing_address */
    $billing_address = $billing_profile
      && $billing_profile->hasField('address')
      && !$billing_profile->get('address')->isEmpty() ?
        $billing_profile->address->first() : NULL;

    if ($billing_address) {
      $request_params['customer.givenName'] = $billing_address->getGivenName();
      $request_params['customer.surname'] = $billing_address->getFamilyName();

      if ($company = $billing_address->getOrganization()) {
        $request_params['customer.companyName'] = $company;
      }

      $request_params['billing.street1'] = $billing_address->getAddressLine1();
      $request_params['billing.street2'] = $billing_address->getAddressLine2();
      $request_params['billing.city'] = $billing_address->getLocality();
      $request_params['billing.postcode'] = $billing_address->getPostalCode();
      $request_params['billing.country'] = $billing_address->getCountryCode();
    }

    $shipping_profile = $this->shippingProfileResolver->resolve($order);
    /** @var \Drupal\address\AddressInterface|null $shipping_address */
    $shipping_address = $shipping_profile
    && $shipping_profile->hasField('address')
    && !$shipping_profile->get('address')->isEmpty() ?
      $shipping_profile->address->first() : NULL;

    if ($shipping_address) {
      $request_params['shipping.customer.email'] = $order->getEmail();
      $request_params['shipping.customer.ip'] = $order->getIpAddress();
      $request_params['shipping.customer.givenName'] = $shipping_address->getGivenName();
      $request_params['shipping.customer.surname'] = $shipping_address->getFamilyName();

      if ($company = $shipping_address->getOrganization()) {
        $request_params['shipping.customer.companyName'] = $company;
      }

      $request_params['shipping.street1'] = $shipping_address->getAddressLine1();
      $request_params['shipping.street2'] = $shipping_address->getAddressLine2();
      $request_params['shipping.city'] = $shipping_address->getLocality();
      $request_params['shipping.postcode'] = $shipping_address->getPostalCode();
      $request_params['shipping.country'] = $shipping_address->getCountryCode();
    }

    $checkout_id = $opp_gateway->prepareCheckout($request_params, $payment);

    $is_sibs_multibanco = $opp_gateway instanceof CopyAndPaySibsMultibanco;
    if (!$is_sibs_multibanco) {
      $payment->setExpiresTime($opp_gateway->calculateCheckoutIdExpireTime());
    }
    $payment->getState()->applyTransitionById('authorize');
    $payment->save();

    $script_url = sprintf("%s/v1/paymentWidgets.js?checkoutId=%s", $opp_gateway->getActiveHostUrl(), $checkout_id);
    $js_settings = [
      'opp_script_url' => $script_url,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
    ];
    if ($opp_gateway instanceof CopyAndPaySofortueberweisungInterface) {
      $sofort_countries = $opp_gateway->getSofortCountries();
      if ($opp_gateway->isSofortRestrictedToBillingAddress() && !empty($billing_address)) {
        $restrict_countries = [strtoupper($billing_address->getCountryCode())];
        $restrict_countries = array_combine($restrict_countries, $restrict_countries);
        $sofort_countries = array_intersect_key($sofort_countries, $restrict_countries);
      }
      $js_settings['sofort_countries'] = (object) $sofort_countries;
    }
    $form['#attached']['drupalSettings']['commerce_opp'] = $js_settings;
    $form['#attached']['library'][] = 'commerce_opp/init';

    $amount_formatted = '';
    if ($opp_gateway->isAmountVisible()) {
      $amount_formatted = $this->t('Amount to be paid: @amount', [
        '@amount' => $this->currencyFormatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
      ]);
    }

    $form['cards'] = [
      '#type' => 'hidden',
      '#value' => implode(' ', $brand_ids),
      // Plugin forms are embedded using #process, so it's too late to attach
      // another #process to $form itself, it must be on a sub-element.
      '#process' => [
        [get_class($this), 'processCopyAndPayForm'],
      ],
      '#action' => $form['#return_url'],
      '#cancel_url' => $form['#cancel_url'],
      '#amount' => $amount_formatted,
    ];

    // No need to call buildRedirectForm(), as we embed an iframe.
    return $form;
  }

  /**
   * Prepares the complete form in order to work with PAYFRAME.
   *
   * Sets the form #action, adds a class for the JS to target.
   * Workaround for buildConfigurationForm() not receiving $complete_form.
   *
   * @param array $element
   *   The form element whose value is being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processCopyAndPayForm(array $element, FormStateInterface $form_state, array &$complete_form) {
    $complete_form['#action'] = $element['#action'];
    $complete_form['#attributes']['class'][] = 'paymentWidgets';
    $complete_form['#attributes']['data-brands'] = $element['#value'];

    if (!empty($element['#amount'])) {
      $complete_form['#prefix'] = $element['#amount'];
    }

    // As the PAYFRAME fully replaces the HTML form, we need to place the cancel
    // link outside the form as suffix.
    $complete_form['#suffix'] = Link::fromTextAndUrl(t('Cancel'), Url::fromUri($element['#cancel_url']))->toString();

    return $element;
  }

}
