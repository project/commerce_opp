<?php

namespace Drupal\commerce_opp;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Represents a supported brand.
 */
final class Brand {

  /**
   * The "Bank Account Brands" type.
   */
  const TYPE_BANK = 'bank';

  /**
   * The "Card Account Brands" type.
   */
  const TYPE_CARD = 'card';

  /**
   * The "Virtual Account Brands" type.
   */
  const TYPE_VIRTUAL = 'virtual';

  /**
   * The brand ID used by Open Payment Platform.
   *
   * @var string
   */
  protected $id;

  /**
   * The brand ID used by commerce_payment, if available (Credit cards only).
   *
   * @var string
   */
  protected $commerceId;

  /**
   * The type of the brand: one of "bank", "card", "virtual".
   *
   * @var string
   */
  protected $type;

  /**
   * The brand label.
   *
   * @var string
   */
  protected $label;

  /**
   * The payment type to use.
   *
   * @var string
   */
  protected $paymentType;

  /**
   * Whether the workflow is sync/async.
   *
   * @var bool
   */
  protected $sync = FALSE;

  /**
   * Whether there is a dedicated gateway plugin for this brand.
   *
   * @var bool
   */
  protected $dedicatedPlugin = FALSE;

  /**
   * Constructs a new Brand instance.
   *
   * @param array $definition
   *   The definition.
   */
  public function __construct(array $definition) {
    foreach (['id', 'label', 'type'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new \InvalidArgumentException(new TranslatableMarkup('Missing required property @property.', [
          '@property' => $required_property,
        ]));
      }
    }

    $this->id = $definition['id'];
    $this->label = $definition['label'];
    $this->paymentType = $definition['payment_type'] ?? PaymentTypes::DEBIT;
    $this->type = $definition['type'];

    if (isset($definition['commerce_id'])) {
      $this->commerceId = $definition['commerce_id'];
    }

    if (isset($definition['sync'])) {
      $this->sync = $definition['sync'];
    }

    if (isset($definition['dedicated_plugin'])) {
      $this->dedicatedPlugin = $definition['dedicated_plugin'];
    }
  }

  /**
   * Gets the brand ID used by Open Payment Platform.
   *
   * @return string
   *   The brand ID used by Open Payment Platform.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Gets the brand label.
   *
   * @return string
   *   The brand label.
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Get the brand ID used by commerce_payment, if available (credit cards).
   *
   * @return string
   *   The brand ID used by commerce_payment, if available (credit cards only).
   */
  public function getCommerceId() {
    return $this->commerceId;
  }

  /**
   * Gets the brand type.
   *
   * @return string
   *   The brand type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Gets the payment type.
   *
   * @return string
   *   The payment type.
   */
  public function getPaymentType() {
    return $this->paymentType;
  }

  /**
   * Returns whether the workflow is sync or async..
   *
   * @return bool
   *   TRUE, if the workflow is sync, FALSE otherwise.
   */
  public function isSync() {
    return $this->sync;
  }

  /**
   * Returns whether the brand has a dedicated gateway plugin.
   *
   * @return bool
   *   TRUE, if the brand has a dedicated gateway plugin, FALSE otherwise.
   */
  public function isDedicatedPlugin() {
    return $this->dedicatedPlugin;
  }

  /**
   * Gets the array representation of the brand.
   *
   * @return array
   *   The array representation of the brand.
   */
  public function toArray() {
    $values = [
      'id' => $this->getId(),
      'label' => $this->getLabel(),
      'type' => $this->getType(),
      'payment_type' => $this->getPaymentType(),
      'sync' => $this->isSync(),
    ];
    if (!empty($this->commerceId)) {
      $values['commerce_id'] = $this->commerceId;
    }
    if (isset($this->dedicated_plugin)) {
      $values['dedicated_plugin'] = $this->dedicatedPlugin;
    }
    return $values;
  }

}
