(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.oppCheckTransactionStatus = {
    attach: function (context, settings) {
      setInterval(function() {
        $.getJSON(settings.commerce_opp.check_status_url, function(data) {
          if (data.status !== 'pending') {
            location.reload(true);
          }
        });
      }, 35000);
    }
  };
})(jQuery, Drupal);
