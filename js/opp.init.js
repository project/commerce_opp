var wpwlOptions;

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.initOppPayment = {
    attach: function (context) {
      let $form = $(context).find('.paymentWidgets');

      if ($form.length > 0) {
        let formElements = once('opp-payment-form', $form.get(0));
        $(formElements).each(function () {
          let opp_settings = drupalSettings.commerce_opp;
          wpwlOptions = {
            locale: opp_settings.langcode
          };
          if (opp_settings.sofort_countries) {
            wpwlOptions.sofortCountries = opp_settings.sofort_countries;
          }
          let opp = document.createElement('script');
          opp.type = 'text/javascript';
          opp.src = opp_settings.opp_script_url;
          let s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(opp, s);
        });
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
